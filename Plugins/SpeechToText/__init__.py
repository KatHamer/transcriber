"""
Transcriber BotPlugin
BotPlugin that transcribes voice notes to readable text messages
By Katelyn Hamer
"""

import logging
from telethon import events, types
from config import settings
from pathlib import Path
import speech_recognition
from pydub import AudioSegment
from Plugins import utils
import os

logger = logging.getLogger("speech_to_text")
RECOGNISER = speech_recognition.Recognizer()

with open("google_cloud_credentials.json") as fp:
    GOOGLE_CLOUD_JSON = (
        fp.read()
    )  # Google Cloud Speech expects credentials as a string representation of the json file you can download from the console, so we explicitly load it


def transcribe_audio(audio_path: Path) -> str:
    """
    Transcribe audio to text using speech_recognition
    args:
    path - PathLike to WAV file containing audio to transcribe
    """
    with speech_recognition.AudioFile(str(audio_path.resolve())) as source:
        try:
            audio_data = RECOGNISER.record(source)
            transcription = RECOGNISER.recognize_google_cloud(
                audio_data,
                credentials_json=GOOGLE_CLOUD_JSON,
                language="en-GB"
            #    auto_punctuation=True,
            )
        except speech_recognition.UnknownValueError:
            return None

    return transcription


@events.register(events.NewMessage(pattern="\/stt(@tr4nscr1b3r_bot)?"))
async def speech_to_text(event):
    """Convert speech to text"""
    if (
        reply_message := await event.get_reply_message()
    ):  # Get the message the event is replying to, if any
        if reply_message.voice:  # Verify that the message is a voice note
            audio_duration = reply_message.document.attributes[
                0
            ].duration  # I'm not sure if the voice note attribute is always the first in the attibute list, this might lead to bugs later on
            audio_duration_human = utils.human_time_duration(audio_duration)
            if audio_duration > settings.speech_to_text.max_voice_note_duration:
                max_duration_human = utils.human_time_duration(  # Convert the max audio duration config value into a human readable time string
                    settings.speech_to_text.max_voice_note_duration
                )
                await event.reply(
                    f"Sorry, I can only transcribe voice notes under {max_duration_human}!"
                )
                return
            logger.debug(
                f"{event.sender.id} requested transcription of message id {reply_message.id}!"
            )
            initial_reply = await event.reply(
                "Transcribing voice note, please wait a moment..."
            )  # Message object used for status updates and the final result

            file_path = Path(
                settings.speech_to_text.download_directory,
                reply_message.file.id + reply_message.file.ext,
            )

            logger.debug("Downloading voice message...")
            await reply_message.download_media(file_path)  # Download the voice message
            logger.debug(
                f"Downloaded voice note from message {reply_message.id} to {file_path}!"
            )

            # Convert the voice note from ogg to wav for transcription
            logger.debug("Converting audio...")
            wav_path = Path(file_path.parent, file_path.stem + ".wav")
            AudioSegment.from_ogg(file_path).export(wav_path, format="wav")
            
            # Delete the original oga file
            logger.debug(f"Deleting original voice note: {file_path.resolve()}")
            os.remove(file_path)

            # Transcribe the audio
            # TODO Split long transcriptions up into multiple messages
            logger.debug("Transcribing audio...")
            transcription = transcribe_audio(wav_path)

            # Delete the converted wav file
            logger.debug(f"Deleting converted audio note: {wav_path.resolve()}")
            os.remove(wav_path)
            
            if transcription:
                await initial_reply.edit(
                    f"Voice note from {utils.get_friendly_name(reply_message.sender)}: **(Duration: {audio_duration_human})**\n\n{transcription}"
                )
            else:
                await initial_reply.edit(
                    f"Sorry, I couldn't understand the voice note."
                )
        else:
            await event.reply("Reply to a voice note to transcribe it!")
    else:
        await event.reply("Reply to a voice note to transcribe it!")


def init_event_handlers(bot):
    """Initialise event handlers"""
    bot.client.add_event_handler(speech_to_text)
