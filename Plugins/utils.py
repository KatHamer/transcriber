import logging
import json
from config import settings
import re
from typing import Tuple, Union
from pathlib import Path

logger = logging.getLogger(__name__)

TIME_DURATION_UNITS = (
    ("week", 60 * 60 * 24 * 7),
    ("day", 60 * 60 * 24),
    ("hour", 60 * 60),
    ("minute", 60),
    ("second", 1),
)


def get_friendly_name(user):
    """Get a friendly nickname for a user"""
    if user_friendly_name := settings.get_fresh("known_users").get(str(user.id)):
        logging.debug(
            f"Using friendly name for user id {user.id} = {user_friendly_name}"
        )
    else:
        logging.debug(f"User id {user.id} has no friendly name")
        user_friendly_name = user.first_name if user.first_name else "friend"
    return user_friendly_name


def human_time_duration(seconds: int) -> str:
    """Convert seconds to human readable time string"""
    if seconds == 0:
        return "inf"
    parts = []
    for unit, div in TIME_DURATION_UNITS:
        amount, seconds = divmod(int(seconds), div)
        if amount > 0:
            parts.append("{} {}{}".format(amount, unit, "" if amount == 1 else "s"))
    return ", ".join(parts)


def parse_lock_file(lock_file_path: Union[Path, str]) -> Tuple[str, str, str]:
    """
    Parse lock file to get current pid, version, and init epoch
    Returns None if lock file is missing/malformed
    """
    # e.g. 4.20.1.1620669805:14281 ->  14281, 4.2.1, 1620669805

    lock_file_path = Path(lock_file_path)

    if not lock_file_path.exists():
        logger.warning("Lock file does not exist!")
        return None

    with open(lock_file_path) as fp:
        lock_file_string = fp.read()
        logger.debug(f"Read lock file: {lock_file_string}")
    if match := re.match(
        "^([0-9]+\.[0-9]+\.[0-9]+)\.(..+):([0-9]+)$", lock_file_string
    ):
        version, init_epoch, pid = match.groups()
    else:
        logger.warning("Contents of lock file are malformed!")
        return None

    return pid, version, int(init_epoch)


def create_user_link(user):
    """Helper function to create user link"""
    user_friendly_name = get_friendly_name(user)
    return f"[{user_friendly_name}](tg://user?id={user.id})"


def timestamped_version_string(init_epoch):
    """Returns current running version + init timestamp"""
    return f"{settings.version}.{int(init_epoch)}"
