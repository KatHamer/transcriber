# Transcriber

Telegram bot that transcribes voice notes to readable text messages.

## Setup

- `git clone https://gitlab.com/KatHamer/transcriber`
- Create a virtual Python environment: `python -m venv venv`
- Create the log and session directories: `mkdir Sessions` and `mkdir Logs`
- Activate the venv: `source venv/bin/activate`
- Install requirements: `pip install -r requirements.txt`
- Obtain a bot token from BotFather and your Telegram API credentials
- Copy the example configurations from `ExampleConfigs` into the project root
- Fill in Telegram credentials in `.secrets.toml`
- Download your Google Cloud credential json by following [these steps](https://cloud.google.com/docs/authentication/getting-started) (you must already have Google Speech Cloud set up in a new project)


- Optionally, change the hardcoded `BOT_DIR` variable in bin/transcriberbot and copy it to /usr/bin, you can then configure a service to call it so that the bot starts on boot. If you use SystemD, you can use the included service file (make sure to change the User parameter to your host's user)

## Usage

Reply to a voice note with the command `/stt` (or whatever you set in `Plugins/SpeechToText/__init__.py`).
