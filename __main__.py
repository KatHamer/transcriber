"""
Transcriber - A bot for transcribing voice notes
Based on TranscriberBot
By Katelyn Hamer <3
"""

import json
import logging
import time
from logging import getLogger, handlers
from pathlib import Path
import os
import signal
import coloredlogs
from telethon.tl import functions, types
from telethon import TelegramClient

import PluginManager
from config import settings  # Local config file
from Plugins import utils

# We store the epoch at which the bot was first initialised, version strings are suffixed with this number
INIT_TIME = time.time()
TIMESTAMPED_VERSION_STRING = utils.timestamped_version_string(INIT_TIME)

# Set up logging
if settings.enable_logging:
    logging.basicConfig(
        level=logging.DEBUG,
        format="%(asctime)s [%(levelname)s] %(message)s",
        handlers=[
            logging.StreamHandler(),
            logging.FileHandler(Path(settings.log_file_directory, settings.log_file_name))
        ],
    )
    logger = getLogger(__name__)
    
    logging.getLogger("transcriber_pluginmanager").setLevel(logging.DEBUG)
    logging.getLogger("transcriber_plugin").setLevel(logging.DEBUG)

    if settings.enable_colored_logging:
        coloredlogs.install(
            level="DEBUG",
            fmt="%(asctime)s [%(levelname)s] %(name)s %(message)s",
        )

    logging.getLogger("telethon").setLevel(
        logging.ERROR
    )  # We only want to log errors from Telethon
else:
    print("Note: Logging is disabled.")

# We use a lock-file to prevent multiple instances from being run
LOCK_FILE_PATH = Path(settings.lock_file_path)

# We check if a lock-file is already present, if it is, we bail to avoid duplicate instances
# This logic must happen before the client is initialised to avoid disturbing already existing session files
if LOCK_FILE_PATH.exists():
    with open(LOCK_FILE_PATH, "r") as fp:
        version_string, process_pid = fp.read().split(
            ":"
        )  # Lock files have format of 4.2.0.023289322398:3605
        logging.warning(
            f"Bailing due to an existing instance ({version_string=}, {process_pid=})"
        )
        print(
            f"Error: TranscriberBot {version_string} is already running (PID: {process_pid}), if you are sure an instance is not active you can delete {LOCK_FILE_PATH.name}."
        )  # If there is a lock file already we discard any attempt to initialise and quit
        exit(1)  # Fuck this shit I'm out
else:
    with open(
        LOCK_FILE_PATH, "w+"
    ) as fp:  # If there isn't a lock file present, we continue execution and create one for the current process
        logging.debug(f"Created new lock file - {LOCK_FILE_PATH.resolve()}")
        fp.write(f"{TIMESTAMPED_VERSION_STRING}:{os.getpid()}")


# Initialise the client
client = TelegramClient(
    api_id=settings.telegram.api_id,
    api_hash=settings.telegram.api_hash,
    session=settings.telegram.session_name,
)


class Bot:
    def __init__(self, client):
        self.client = client
        self.plugin_directory = Path(settings.plugin_directory)
        self.plugin_manager = PluginManager.PluginManager(self)
        self.init_time = INIT_TIME
        self.timestamped_version_string = TIMESTAMPED_VERSION_STRING
        plugins = self.plugin_manager.load_all()
        formatted_plugin_list = ", ".join(plugins.keys())
        logging.info(f"Loaded plugins: {formatted_plugin_list}")

    def event_loop(self):
        """Initialises Telethon event loop"""
        client.start(bot_token=settings.telegram.bot_token)
        client.run_until_disconnected()

# Set up a handler to handler SIGTERM, i.e. from systemd
handle_sigterm = lambda signal, frame: exit(0)  # Raise a SystemExit when we get a sigterm
signal.signal(signal.SIGTERM, handle_sigterm)

if __name__ == "__main__":
    TranscriberBot = Bot(client)
    logger.info(f"TranscriberBot {TIMESTAMPED_VERSION_STRING} starting...")
    try:
        TranscriberBot.event_loop()  # This is blocking
    except KeyboardInterrupt:
        logger.info("Deactivating due to Ctrl^C")
    except SystemExit:
        logger.info("Deactivating due to SystemExit")
    except Exception as exc:
        logger.error("Unhandled exception occured in main event loop:")
        logger.exception(exc)
    finally:
        client.disconnect()
        logger.warning(
            f"TranscriberBot {TIMESTAMPED_VERSION_STRING} is now inactive. Deleting lock-file."
        )
        logger.debug(f"Deleting {LOCK_FILE_PATH.resolve()}")
        os.remove(LOCK_FILE_PATH.resolve())  # Delete lock file

