"""
PluginManager
Plugin manager for SchBot
By Katelyn Hamer <3
"""

import yaml
import logging
import importlib
from pathlib import Path

logger = logging.getLogger("schbot_pluginmanager")
logger.setLevel("INFO")


class PluginManager:
    def __init__(self, bot):
        self.bot = bot
        self.plugin_directory = bot.plugin_directory

    def load_all(self):
        """Load all plugins in a specified directory"""
        logger.info(f"Loading all plugins in {self.plugin_directory}...")
        if self.plugin_directory.exists():
            plugin_map = {}
            potential_plugins = (
                item
                for item in self.plugin_directory.iterdir()
                if item.is_dir() and not item.name[0] == "_"
            )
            for plugin in potential_plugins:
                logger.debug(f"Attempting to load {plugin.name}...")
                init_path = Path(plugin, "__init__.py")
                metadata_path = Path(plugin, "metadata.yml")
                if init_path.exists() and metadata_path.exists():
                    plugin_metadata, plugin_import = self.load_plugin(plugin)
                    if plugin_metadata and plugin_import:
                        plugin_map[plugin.name] = {
                            "metadata": plugin_metadata,
                            "plugin": plugin_import,
                        }
                else:
                    logger.warning(f"Directory {plugin} is not a valid plugin!")
            return plugin_map

    def load_plugin(self, plugin_directory_name):
        """Load a plugin"""
        plugin_path = Path(plugin_directory_name)
        if plugin_path.exists():
            metadata_path = Path(plugin_path, "metadata.yml")
            if metadata_path.exists():
                with open(metadata_path) as file:
                    metadata = yaml.safe_load(file)
                    plugin_name = metadata["name"]
                    # logger.debug(f"Plugin metadata for {plugin_name}: {metadata}.")
                    # logger.info(f"Loaded plugin {plugin_name}.")
                    imported_plugin = importlib.import_module(
                        f"Plugins.{plugin_name}"
                    )  # TODO Allow arbitrary plugin dir
                    logger.debug(f"Imported plugin: {imported_plugin}")
                    if hasattr(imported_plugin, "init_event_handlers"):
                        logger.debug(
                            f"Plugin {plugin_name} reports having event handlers. Registering..."
                        )
                        imported_plugin.init_event_handlers(self.bot)
                    return imported_plugin, metadata
        else:
            logger.error(f"Plugin directory {plugin_path} does not exist.")
            return None, None
